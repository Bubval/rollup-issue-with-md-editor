# Rollup Bug Demonstration with `@uiw/react-md-editor`

## Description

This project is a minimal setup to reproduce a bug with Rollup and `@uiw/react-md-editor`. The issue arises when adding the `@uiw/react-md-editor` package to a clean TypeScript-React project.

## Prerequisites

- Node.js
- npm

## Installation

Clone this repo and then run the following command to install all dependencies:

```bash
npm install
```

## Issue Details

### Steps to Reproduce

1. Clone this repository
2. Run `npm install`
3. Run `npm run rollup`

### Expected Result

Rollup should bundle the project without errors.

### Actual Result

Rollup throws an error:

```bash
[!] RollupError: Expected '{', got 'interface' (Note that you need plugins to import files that are not JavaScript)
src/components/Button.tsx (3:7)
1: import React from 'react';
2:
3: export interface ButtonProps {
^
4: label: string;
5: }
```

### Isolating the Issue

To make the project run without errors, you can:

1. Comment out the content in `src/Editor.tsx`.

This will allow Rollup to bundle the project successfully.

## Contributing

If you have any insights or solutions regarding this issue, feel free to submit a PR or create an issue.
